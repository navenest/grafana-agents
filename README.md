# grafana-agents



## Getting started

1. (Install OhmGraphite)[https://github.com/nickbabcock/OhmGraphite/blob/master/README.md#installation]


### OHM Installation

- Create a directory that will be the home base for OhmGraphite (I use `C:\Apps\OhmGraphite`).
- Download the [latest zip](https://github.com/nickbabcock/OhmGraphite/releases/latest) and extract to our directory.
- Update app configuration (located at `OhmGraphite.exe.config`). See configs for [Graphite](#graphite-configuration), [InfluxDB](#influxdb-configuration), [Prometheus](#prometheus-configuration), [Timescale / Postgres](#timescaledb-configuration)
- To install the app `.\OhmGraphite.exe install`. The command will install OhmGraphite as a Windows service (so you can manage it with your favorite powershell commands or `services.msc`)
- To start the app after installation: `.\OhmGraphite.exe start` or your favorite Windows service management tool
- If immediately installing the app is unnerving, the app can be ran interactively by executing `.\OhmGraphite.exe run`. Executing as administrator will most likely increase the number of sensors found (OhmGraphite will log how many sensors are found).


### OHM Extra

1. Replace OhmGraphite.exe.config with the one in this repo (Setups prometheus scrapable endpoint)
1. Recycle OhmGraphite Service

1. (Install Grafana Agent)[https://grafana.com/docs/agent/latest/set-up/install-agent-on-windows/#install-grafana-agent-on-windows]
1. Update agent-config.yaml with one in this repo
1. Recycle grafana agent
